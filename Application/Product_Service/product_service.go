// Package main implements a server for movieinfo service.
package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"time"

	"product-service/product_service_api"
	"product-service/user_service_api"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
)

const (
	port        = ":8001"
	useraddress = "user-service:8000"
)

// server is used to implement marketapi.MarketInfoServer
type server struct {
	product_service_api.UnimplementedProductServer
}

const (
	//mongodbEndpoint = "mongodb://172.17.0.2:27017" // Find this from the Mongo container
	mongodbEndpoint = "mongodb://mongo:27017"
)

type proD struct {
	Name           string
	Price          float32
	SellerName     string
	SellerUsername string
	Contact        string
	Location       string
	Availability   string
	ProductDesc    string
	ProductId      string
}

// Map representing a database

// AddProduct implements movieapi.MovieInfoServer
func (s *server) AddProduct(ctx context.Context, in *product_service_api.ProductDetails) (*product_service_api.Status, error) {
	client, col := dbconnection()
	defer disconnect(client)
	status := &product_service_api.Status{}
	conn, err := connectgrpc(useraddress)
	defer grpcdisconnect(conn)
	if err != nil {
		return nil, err
	}
	c := user_service_api.NewUserserviceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	name, error := c.GetName(ctx, &user_service_api.Username{Username: in.GetSellerUsername()})
	defer cancel()
	if error != nil {
		status.Code = "Username not valid"
		return status, nil
	}

	product := &proD{in.GetName(), in.GetPrice(), name.Name, in.GetSellerUsername(), in.GetContact(), in.GetLocation(), in.GetAvailability(), in.GetProductDesc(), in.GetProductID()}
	log.Printf("Received: %v", product.Name)
	col.InsertOne(context.TODO(), product)
	status.Code = "Success"
	return status, nil

}

func (s *server) BrowseProduct(ctx context.Context, in *product_service_api.ProductData) (*product_service_api.ProductList, error) {
	client, col := dbconnection()
	defer disconnect(client)
	location := in.GetLocation()
	name := in.GetName()
	reply := &product_service_api.ProductList{}
	var r []*product_service_api.ProductDetails
	var result []*proD
	var filter bson.D
	if location == "0" && name == "0" {
		filter = bson.D{{"availability", "True"}}
		log.Printf("no location and name")
	} else if location == "0" {
		filter = bson.D{{"name", name}, {"availability", "True"}}
		log.Printf("No location")
	} else if name == "0" {
		filter = bson.D{{"location", location}, {"availability", "True"}}
		log.Printf("No name")
	} else {
		filter = bson.D{{"location", location}, {"name", name}, {"availability", "True"}}
	}
	cur, err := col.Find(context.TODO(), filter)
	if err != nil {
		return nil, err
	}
	for cur.Next(context.TODO()) {
		//fmt.Println("hi")
		// create a value into which the single document can be decoded
		var elem proD
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		log.Println(elem)
		result = append(result, &elem)
	}
	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}
	for _, v := range result {
		re := &product_service_api.ProductDetails{Name: v.Name, Price: v.Price, SellerName: v.SellerName, SellerUsername: v.SellerUsername, Contact: v.Contact, Location: v.Location, Availability: v.Availability, ProductDesc: v.ProductDesc, ProductID: v.ProductId}
		r = append(r, re)

	}
	reply.Res = r
	return reply, nil

}

func (s *server) MyProducts(ctx context.Context, in *product_service_api.Username) (*product_service_api.ProductList, error) {
	client, col := dbconnection()
	defer disconnect(client)
	username := in.GetUsername()
	reply := &product_service_api.ProductList{}
	var r []*product_service_api.ProductDetails
	var result []*proD
	filter := bson.D{{"sellerusername", username}}
	cur, err := col.Find(context.TODO(), filter)
	if err != nil {
		return nil, err
	}
	for cur.Next(context.TODO()) {
		//fmt.Println("hi")
		// create a value into which the single document can be decoded
		var elem proD
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		//fmt.Println(elem)
		result = append(result, &elem)
	}
	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}
	for _, v := range result {
		re := &product_service_api.ProductDetails{Name: v.Name, Price: v.Price, SellerName: v.SellerName, Contact: v.Contact, Location: v.Location, Availability: v.Availability, ProductDesc: v.ProductDesc, ProductID: v.ProductId}
		r = append(r, re)

	}
	reply.Res = r
	return reply, nil
}

func (s *server) SoldProduct(ctx context.Context, in *product_service_api.ProductId) (*product_service_api.Status, error) {
	client, col := dbconnection()
	defer disconnect(client)
	reply := &product_service_api.Status{}

	filter := bson.D{{"productid", in.GetId()}}
	update := bson.D{
		{"$set", bson.D{
			{"availability", "False"},
		}},
	}
	reply.Code = "Success"
	updateResult, err := col.UpdateOne(context.TODO(), filter, update)
	if updateResult.MatchedCount == 0 {
		reply.Code = "Fail: Item not present"
	}
	if err != nil {
		reply.Code = "Fail"
	}

	return reply, err
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	product_service_api.RegisterProductServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
	log.Printf("Exited")
}

func dbconnection() (*mongo.Client, *mongo.Collection) {
	// Set client options
	clientOptions := options.Client().ApplyURI(mongodbEndpoint)

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	// Get a handle for your collection
	col := client.Database("store").Collection("items")
	return client, col
}

func disconnect(client *mongo.Client) {
	client.Disconnect(context.TODO())
	fmt.Println("Disconnected to MongoDB!")
}

func connectgrpc(address string) (*grpc.ClientConn, error) {
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	return conn, err
}
func grpcdisconnect(conn *grpc.ClientConn) {
	conn.Close()
}
