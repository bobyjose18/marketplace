// Package main implements a server for movieinfo service.
package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net"

	"user-service/user_service_api"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
)

const (
	port = ":8000"
)

// server is used to implement marketapi.MarketInfoServer
type server struct {
	user_service_api.UnimplementedUserserviceServer
}

const (
	//mongodbEndpoint = "mongodb://172.17.0.2:27017" // Find this from the Mongo container
	mongodbEndpoint = "mongodb://mongo:27017"
)

type User struct {
	Name     string
	Username string
	Password string
	contact  string
}

// Map representing a database

// AddProduct implements movieapi.MovieInfoServer
func (s *server) AddUser(ctx context.Context, in *user_service_api.User) (*user_service_api.SignupResponse, error) {
	client, col := dbconnection()
	defer disconnect(client)
	resp := &user_service_api.SignupResponse{}
	resp.Status = "Success"
	u := &User{in.GetName(), in.GetUsername(), in.GetPassword(), in.GetContact()}
	log.Printf("Received: %v", u.contact)
	_, err := col.InsertOne(context.TODO(), u)
	return resp, err
}

func (s *server) Login(ctx context.Context, in *user_service_api.Creden) (*user_service_api.LoginResponse, error) {
	client, col := dbconnection()
	defer disconnect(client)
	username := in.GetUsername()
	password := in.GetPassword()
	filter := bson.D{{"username", username}}
	log.Printf("Received: %v", username)
	var u User
	res := col.FindOne(context.TODO(), filter)
	if res.Err() != nil {
		err := errors.New("Failed")
		return nil, err
	}
	res.Decode(&u)
	fmt.Println(u.Name, u.Password)
	resp := &user_service_api.LoginResponse{}
	resp.Name = u.Name
	resp.Username = u.Username
	if u.Password != password {
		resp.Name = "nil"
	}
	return resp, nil
}

func (s *server) CheckUsername(ctx context.Context, in *user_service_api.Username) (*user_service_api.Status, error) {
	client, col := dbconnection()
	defer disconnect(client)
	username := in.GetUsername()
	filter := bson.D{{"username", username}}
	log.Printf("Received: %v", username)
	var u User
	res := col.FindOne(context.TODO(), filter)
	if res.Err() != nil {
		return nil, res.Err()
	}
	res.Decode(&u)
	stat := &user_service_api.Status{}
	stat.Status = "Failure"
	return stat, nil
}

func (s *server) GetPhoneNumber(ctx context.Context, in *user_service_api.Username) (*user_service_api.Phonenumber, error) {
	client, col := dbconnection()
	defer disconnect(client)
	username := in.GetUsername()
	filter := bson.D{{"username", username}}
	log.Printf("Received: %v", username)
	var u User
	res := col.FindOne(context.TODO(), filter)
	if res.Err() != nil {
		return nil, res.Err()
	}
	res.Decode(&u)
	log.Printf(u.Name, u.Username)
	log.Printf("\n", u.contact)
	number := &user_service_api.Phonenumber{}
	number.Contact = u.contact
	return number, nil
}

func (s *server) GetName(ctx context.Context, in *user_service_api.Username) (*user_service_api.Name, error) {
	client, col := dbconnection()
	defer disconnect(client)
	username := in.GetUsername()
	filter := bson.D{{"username", username}}
	log.Printf("Received: %v", username)
	var u User
	res := col.FindOne(context.TODO(), filter)
	if res.Err() != nil {
		return nil, res.Err()
	}
	res.Decode(&u)
	name := &user_service_api.Name{}
	name.Name = u.Name
	return name, nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	user_service_api.RegisterUserserviceServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func dbconnection() (*mongo.Client, *mongo.Collection) {
	// Set client options
	clientOptions := options.Client().ApplyURI(mongodbEndpoint)

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	// Get a handle for your collection
	col := client.Database("store").Collection("User")
	return client, col
}

func disconnect(client *mongo.Client) {
	client.Disconnect(context.TODO())
	fmt.Println("Disconnected to MongoDB!")
}
