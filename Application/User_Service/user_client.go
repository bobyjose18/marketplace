// Package main imlements a client for movieinfo service
package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"user-service/user_service_api"

	"google.golang.org/grpc"
)

const (
	address = "localhost:8000"
)

//func (v *marketapi.ProductDetails) String() string { return fmt.Sprintf(v.ProductName) }

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := user_service_api.NewUserserviceClient(conn)

	// Contact the server and print out its response.
	name := "John Doe"
	username := "jd23"
	password := "jd23"
	var contact int64 = 7777777777

	// Timeout if server doesn't respond
	ctx1, cancel1 := context.WithTimeout(context.Background(), time.Second)
	r, err := c.AddUser(ctx1, &user_service_api.User{Name: name, Username: username, Password: password, Contact: contact})
	if err != nil {
		log.Fatalf("could not add user: %v", err)
	}
	log.Printf("Adding the user was %s", r.GetStatus())
	cancel1()

	ctx2, cancel2 := context.WithTimeout(context.Background(), time.Second)
	result, error := c.Login(ctx2, &user_service_api.Creden{Username: username, Password: password})
	if error != nil {
		log.Fatalf("Login Failed: %v", error)

	}
	fmt.Println(result.Name, result.Username)
	cancel2()

	ctx3, cancel3 := context.WithTimeout(context.Background(), time.Second)
	result, error = c.Login(ctx3, &user_service_api.Creden{Username: username, Password: "Hello"})
	if error != nil {
		log.Fatalf("Login Failed: %v", error)

	}
	fmt.Println(result.Name, result.Username)
	cancel3()

	ctx4, cancel4 := context.WithTimeout(context.Background(), time.Second)
	r, err = c.AddUser(ctx4, &user_service_api.User{Name: name, Username: username, Password: password, Contact: contact})
	if err != nil {
		log.Fatalf("could not add user: %v", err)
	}
	log.Printf(r.GetStatus())
	cancel4()

}
