# github.com/PuerkitoBio/purell v1.1.1
github.com/PuerkitoBio/purell
# github.com/PuerkitoBio/urlesc v0.0.0-20170810143723-de5bf2ad4578
github.com/PuerkitoBio/urlesc
# github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef
github.com/asaskevich/govalidator
# github.com/aws/aws-sdk-go v1.34.28
github.com/aws/aws-sdk-go/aws
github.com/aws/aws-sdk-go/aws/awserr
github.com/aws/aws-sdk-go/aws/awsutil
github.com/aws/aws-sdk-go/aws/client/metadata
github.com/aws/aws-sdk-go/aws/credentials
github.com/aws/aws-sdk-go/aws/endpoints
github.com/aws/aws-sdk-go/aws/request
github.com/aws/aws-sdk-go/aws/signer/v4
github.com/aws/aws-sdk-go/internal/context
github.com/aws/aws-sdk-go/internal/ini
github.com/aws/aws-sdk-go/internal/sdkio
github.com/aws/aws-sdk-go/internal/sdkmath
github.com/aws/aws-sdk-go/internal/shareddefaults
github.com/aws/aws-sdk-go/internal/strings
github.com/aws/aws-sdk-go/internal/sync/singleflight
github.com/aws/aws-sdk-go/private/protocol
github.com/aws/aws-sdk-go/private/protocol/rest
# github.com/docker/go-units v0.4.0
github.com/docker/go-units
# github.com/go-openapi/analysis v0.19.16
github.com/go-openapi/analysis
github.com/go-openapi/analysis/internal
# github.com/go-openapi/errors v0.19.9
## explicit
github.com/go-openapi/errors
# github.com/go-openapi/jsonpointer v0.19.5
github.com/go-openapi/jsonpointer
# github.com/go-openapi/jsonreference v0.19.5
github.com/go-openapi/jsonreference
# github.com/go-openapi/loads v0.20.2
## explicit
github.com/go-openapi/loads
# github.com/go-openapi/runtime v0.19.28
## explicit
github.com/go-openapi/runtime
github.com/go-openapi/runtime/flagext
github.com/go-openapi/runtime/logger
github.com/go-openapi/runtime/middleware
github.com/go-openapi/runtime/middleware/denco
github.com/go-openapi/runtime/middleware/header
github.com/go-openapi/runtime/middleware/untyped
github.com/go-openapi/runtime/security
# github.com/go-openapi/spec v0.20.1
## explicit
github.com/go-openapi/spec
# github.com/go-openapi/strfmt v0.20.0
## explicit
github.com/go-openapi/strfmt
# github.com/go-openapi/swag v0.19.13
## explicit
github.com/go-openapi/swag
# github.com/go-openapi/validate v0.20.1
## explicit
github.com/go-openapi/validate
# github.com/go-stack/stack v1.8.0
github.com/go-stack/stack
# github.com/golang/protobuf v1.5.2
## explicit
github.com/golang/protobuf/proto
github.com/golang/protobuf/ptypes
github.com/golang/protobuf/ptypes/any
github.com/golang/protobuf/ptypes/duration
github.com/golang/protobuf/ptypes/timestamp
# github.com/golang/snappy v0.0.1
github.com/golang/snappy
# github.com/jessevdk/go-flags v1.5.0
## explicit
github.com/jessevdk/go-flags
# github.com/jmespath/go-jmespath v0.4.0
github.com/jmespath/go-jmespath
# github.com/josharian/intern v1.0.0
github.com/josharian/intern
# github.com/klauspost/compress v1.9.5
github.com/klauspost/compress/fse
github.com/klauspost/compress/huff0
github.com/klauspost/compress/snappy
github.com/klauspost/compress/zstd
github.com/klauspost/compress/zstd/internal/xxhash
# github.com/mailru/easyjson v0.7.6
github.com/mailru/easyjson/buffer
github.com/mailru/easyjson/jlexer
github.com/mailru/easyjson/jwriter
# github.com/mitchellh/mapstructure v1.4.1
github.com/mitchellh/mapstructure
# github.com/pkg/errors v0.9.1
github.com/pkg/errors
# github.com/xdg-go/pbkdf2 v1.0.0
github.com/xdg-go/pbkdf2
# github.com/xdg-go/scram v1.0.2
github.com/xdg-go/scram
# github.com/xdg-go/stringprep v1.0.2
github.com/xdg-go/stringprep
# github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d
github.com/youmark/pkcs8
# go.mongodb.org/mongo-driver v1.5.1
## explicit
go.mongodb.org/mongo-driver/bson
go.mongodb.org/mongo-driver/bson/bsoncodec
go.mongodb.org/mongo-driver/bson/bsonoptions
go.mongodb.org/mongo-driver/bson/bsonrw
go.mongodb.org/mongo-driver/bson/bsontype
go.mongodb.org/mongo-driver/bson/primitive
go.mongodb.org/mongo-driver/event
go.mongodb.org/mongo-driver/internal
go.mongodb.org/mongo-driver/mongo
go.mongodb.org/mongo-driver/mongo/address
go.mongodb.org/mongo-driver/mongo/description
go.mongodb.org/mongo-driver/mongo/options
go.mongodb.org/mongo-driver/mongo/readconcern
go.mongodb.org/mongo-driver/mongo/readpref
go.mongodb.org/mongo-driver/mongo/writeconcern
go.mongodb.org/mongo-driver/tag
go.mongodb.org/mongo-driver/version
go.mongodb.org/mongo-driver/x/bsonx
go.mongodb.org/mongo-driver/x/bsonx/bsoncore
go.mongodb.org/mongo-driver/x/mongo/driver
go.mongodb.org/mongo-driver/x/mongo/driver/auth
go.mongodb.org/mongo-driver/x/mongo/driver/auth/internal/gssapi
go.mongodb.org/mongo-driver/x/mongo/driver/connstring
go.mongodb.org/mongo-driver/x/mongo/driver/dns
go.mongodb.org/mongo-driver/x/mongo/driver/mongocrypt
go.mongodb.org/mongo-driver/x/mongo/driver/mongocrypt/options
go.mongodb.org/mongo-driver/x/mongo/driver/ocsp
go.mongodb.org/mongo-driver/x/mongo/driver/operation
go.mongodb.org/mongo-driver/x/mongo/driver/session
go.mongodb.org/mongo-driver/x/mongo/driver/topology
go.mongodb.org/mongo-driver/x/mongo/driver/uuid
go.mongodb.org/mongo-driver/x/mongo/driver/wiremessage
# golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
golang.org/x/crypto/ocsp
golang.org/x/crypto/pbkdf2
# golang.org/x/net v0.0.0-20210119194325-5f4716e94777
## explicit
golang.org/x/net/http/httpguts
golang.org/x/net/http2
golang.org/x/net/http2/hpack
golang.org/x/net/idna
golang.org/x/net/internal/timeseries
golang.org/x/net/netutil
golang.org/x/net/trace
# golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e
golang.org/x/sync/errgroup
golang.org/x/sync/semaphore
# golang.org/x/sys v0.0.0-20210320140829-1e4c9ba3b0c4
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
# golang.org/x/text v0.3.5
golang.org/x/text/secure/bidirule
golang.org/x/text/transform
golang.org/x/text/unicode/bidi
golang.org/x/text/unicode/norm
golang.org/x/text/width
# google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013
google.golang.org/genproto/googleapis/rpc/status
# google.golang.org/grpc v1.37.0
## explicit
google.golang.org/grpc
google.golang.org/grpc/attributes
google.golang.org/grpc/backoff
google.golang.org/grpc/balancer
google.golang.org/grpc/balancer/base
google.golang.org/grpc/balancer/grpclb/state
google.golang.org/grpc/balancer/roundrobin
google.golang.org/grpc/binarylog/grpc_binarylog_v1
google.golang.org/grpc/codes
google.golang.org/grpc/connectivity
google.golang.org/grpc/credentials
google.golang.org/grpc/encoding
google.golang.org/grpc/encoding/proto
google.golang.org/grpc/grpclog
google.golang.org/grpc/internal
google.golang.org/grpc/internal/backoff
google.golang.org/grpc/internal/balancerload
google.golang.org/grpc/internal/binarylog
google.golang.org/grpc/internal/buffer
google.golang.org/grpc/internal/channelz
google.golang.org/grpc/internal/credentials
google.golang.org/grpc/internal/envconfig
google.golang.org/grpc/internal/grpclog
google.golang.org/grpc/internal/grpcrand
google.golang.org/grpc/internal/grpcsync
google.golang.org/grpc/internal/grpcutil
google.golang.org/grpc/internal/metadata
google.golang.org/grpc/internal/resolver
google.golang.org/grpc/internal/resolver/dns
google.golang.org/grpc/internal/resolver/passthrough
google.golang.org/grpc/internal/resolver/unix
google.golang.org/grpc/internal/serviceconfig
google.golang.org/grpc/internal/status
google.golang.org/grpc/internal/syscall
google.golang.org/grpc/internal/transport
google.golang.org/grpc/internal/transport/networktype
google.golang.org/grpc/keepalive
google.golang.org/grpc/metadata
google.golang.org/grpc/peer
google.golang.org/grpc/resolver
google.golang.org/grpc/serviceconfig
google.golang.org/grpc/stats
google.golang.org/grpc/status
google.golang.org/grpc/tap
# google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0
## explicit
# google.golang.org/protobuf v1.26.0
## explicit
google.golang.org/protobuf/encoding/prototext
google.golang.org/protobuf/encoding/protowire
google.golang.org/protobuf/internal/descfmt
google.golang.org/protobuf/internal/descopts
google.golang.org/protobuf/internal/detrand
google.golang.org/protobuf/internal/encoding/defval
google.golang.org/protobuf/internal/encoding/messageset
google.golang.org/protobuf/internal/encoding/tag
google.golang.org/protobuf/internal/encoding/text
google.golang.org/protobuf/internal/errors
google.golang.org/protobuf/internal/filedesc
google.golang.org/protobuf/internal/filetype
google.golang.org/protobuf/internal/flags
google.golang.org/protobuf/internal/genid
google.golang.org/protobuf/internal/impl
google.golang.org/protobuf/internal/order
google.golang.org/protobuf/internal/pragma
google.golang.org/protobuf/internal/set
google.golang.org/protobuf/internal/strs
google.golang.org/protobuf/internal/version
google.golang.org/protobuf/proto
google.golang.org/protobuf/reflect/protodesc
google.golang.org/protobuf/reflect/protoreflect
google.golang.org/protobuf/reflect/protoregistry
google.golang.org/protobuf/runtime/protoiface
google.golang.org/protobuf/runtime/protoimpl
google.golang.org/protobuf/types/descriptorpb
google.golang.org/protobuf/types/known/anypb
google.golang.org/protobuf/types/known/durationpb
google.golang.org/protobuf/types/known/timestamppb
# gopkg.in/yaml.v2 v2.4.0
gopkg.in/yaml.v2
