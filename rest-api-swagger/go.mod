module apiserver

go 1.16

require (
	github.com/go-openapi/errors v0.19.9
	github.com/go-openapi/loads v0.20.2
	github.com/go-openapi/runtime v0.19.28
	github.com/go-openapi/spec v0.20.1
	github.com/go-openapi/strfmt v0.20.0
	github.com/go-openapi/swag v0.19.13
	github.com/go-openapi/validate v0.20.1
	github.com/jessevdk/go-flags v1.5.0
	github.com/rs/cors v1.7.0
	go.mongodb.org/mongo-driver v1.5.1
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.25.0
)
