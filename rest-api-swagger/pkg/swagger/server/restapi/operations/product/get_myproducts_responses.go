// Code generated by go-swagger; DO NOT EDIT.

package product

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"apiserver/pkg/swagger/server/models"
)

// GetMyproductsOKCode is the HTTP code returned for type GetMyproductsOK
const GetMyproductsOKCode int = 200

/*GetMyproductsOK OK

swagger:response getMyproductsOK
*/
type GetMyproductsOK struct {

	/*
	  In: Body
	*/
	Payload models.ProductList `json:"body,omitempty"`
}

// NewGetMyproductsOK creates GetMyproductsOK with default headers values
func NewGetMyproductsOK() *GetMyproductsOK {

	return &GetMyproductsOK{}
}

// WithPayload adds the payload to the get myproducts o k response
func (o *GetMyproductsOK) WithPayload(payload models.ProductList) *GetMyproductsOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get myproducts o k response
func (o *GetMyproductsOK) SetPayload(payload models.ProductList) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetMyproductsOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	payload := o.Payload
	if payload == nil {
		// return empty array
		payload = models.ProductList{}
	}

	if err := producer.Produce(rw, payload); err != nil {
		panic(err) // let the recovery middleware deal with this
	}
}

// GetMyproductsBadRequestCode is the HTTP code returned for type GetMyproductsBadRequest
const GetMyproductsBadRequestCode int = 400

/*GetMyproductsBadRequest Failed to get products

swagger:response getMyproductsBadRequest
*/
type GetMyproductsBadRequest struct {

	/*
	  In: Body
	*/
	Payload string `json:"body,omitempty"`
}

// NewGetMyproductsBadRequest creates GetMyproductsBadRequest with default headers values
func NewGetMyproductsBadRequest() *GetMyproductsBadRequest {

	return &GetMyproductsBadRequest{}
}

// WithPayload adds the payload to the get myproducts bad request response
func (o *GetMyproductsBadRequest) WithPayload(payload string) *GetMyproductsBadRequest {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get myproducts bad request response
func (o *GetMyproductsBadRequest) SetPayload(payload string) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetMyproductsBadRequest) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(400)
	payload := o.Payload
	if err := producer.Produce(rw, payload); err != nil {
		panic(err) // let the recovery middleware deal with this
	}
}
