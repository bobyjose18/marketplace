// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"

	"apiserver/pkg/swagger/server/restapi/operations"
	"apiserver/pkg/swagger/server/restapi/operations/product"
	"apiserver/pkg/swagger/server/restapi/operations/user"
)

//go:generate swagger generate server --target ../../server --name APIServer --spec ../../swagger.yaml --principal interface{} --exclude-main

func configureFlags(api *operations.APIServerAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.APIServerAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	// api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	if api.ProductGetBrowseproductsHandler == nil {
		api.ProductGetBrowseproductsHandler = product.GetBrowseproductsHandlerFunc(func(params product.GetBrowseproductsParams) middleware.Responder {
			return middleware.NotImplemented("operation product.GetBrowseproducts has not yet been implemented")
		})
	}
	if api.UserGetCheckusernameHandler == nil {
		api.UserGetCheckusernameHandler = user.GetCheckusernameHandlerFunc(func(params user.GetCheckusernameParams) middleware.Responder {
			return middleware.NotImplemented("operation user.GetCheckusername has not yet been implemented")
		})
	}
	if api.UserGetLoginHandler == nil {
		api.UserGetLoginHandler = user.GetLoginHandlerFunc(func(params user.GetLoginParams) middleware.Responder {
			return middleware.NotImplemented("operation user.GetLogin has not yet been implemented")
		})
	}
	if api.ProductGetMyproductsHandler == nil {
		api.ProductGetMyproductsHandler = product.GetMyproductsHandlerFunc(func(params product.GetMyproductsParams) middleware.Responder {
			return middleware.NotImplemented("operation product.GetMyproducts has not yet been implemented")
		})
	}
	if api.ProductPostAddproductHandler == nil {
		api.ProductPostAddproductHandler = product.PostAddproductHandlerFunc(func(params product.PostAddproductParams) middleware.Responder {
			return middleware.NotImplemented("operation product.PostAddproduct has not yet been implemented")
		})
	}
	if api.ProductPostSellHandler == nil {
		api.ProductPostSellHandler = product.PostSellHandlerFunc(func(params product.PostSellParams) middleware.Responder {
			return middleware.NotImplemented("operation product.PostSell has not yet been implemented")
		})
	}
	if api.UserPostSignupHandler == nil {
		api.UserPostSignupHandler = user.PostSignupHandlerFunc(func(params user.PostSignupParams) middleware.Responder {
			return middleware.NotImplemented("operation user.PostSignup has not yet been implemented")
		})
	}

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix".
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation.
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics.
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
