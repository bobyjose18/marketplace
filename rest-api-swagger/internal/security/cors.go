package security

import (
	"net/http"

	"github.com/rs/cors"
)

// SetupCORSMiddleware ...
func SetupCORSMiddleware(handler http.Handler) http.Handler {
	corsHandler := cors.New(cors.Options{
		Debug:            true,
		AllowedHeaders:   []string{"*"},
		AllowedOrigins:   []string{"http://localhost:4200", "http://localhost:81", "http://web:81"},
		AllowedMethods:   []string{"GET", "POST", "DELETE", "OPTIONS"},
		AllowCredentials: true,
		ExposedHeaders:   []string{"x-csrf-token"},
		MaxAge:           1000,
	})
	return corsHandler.Handler(handler)
}
